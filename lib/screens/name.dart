import 'package:flutter/material.dart';
import 'package:pendalaman_flutter_skor/models/team.dart';

class TeamName extends StatefulWidget {
  @override
  _TeamNameState createState() => _TeamNameState();
}

class _TeamNameState extends State<TeamName> {
  String teamOneName = 'Team A';
  String teamTwoName = 'Team B';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Nama Team'),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Padding(
                padding: EdgeInsets.all(24),
                child: TextField(
                  // tidak perlu setState karena akan dilempar ke screen lain
                  onChanged: (text) => {teamOneName = text},
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: "Team A"),
                ),
              ),
              Padding(
                padding: EdgeInsets.all(24),
                child: TextField(
                  // tidak perlu setState karena akan dilempar ke screen lain
                  onChanged: (text) => {teamTwoName = text},
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), labelText: "Team B"),
                ),
              ),
              Padding(
                  padding: EdgeInsets.all(24),
                  child: SizedBox(
                      width: double.infinity, // lebar memenuhi parent
                      height: 56,
                      child: ElevatedButton(
                          onPressed: () => {
                                // pindah ke screen baru dan kirim argumen
                                Navigator.pushNamed(context, '/team_score',
                                    arguments: {
                                      'teamOne': Team.named(teamOneName),
                                      'teamTwo': Team.named(teamTwoName)
                                    })
                              },
                          child: Text('START'))))
            ],
          ),
        ));
  }
}
