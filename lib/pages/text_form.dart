import 'package:flutter/material.dart';

import 'content_page.dart';

class TextForm extends StatefulWidget {
  @override
  _TextForm createState() => _TextForm();
}

class _TextForm extends State<TextForm> {
  String kalimat1 = "";
  String kalimat2 = "";
  @override
  Widget build(BuildContext context) {
    // ignore: unused_local_variable
    int angka = 0;
    return Scaffold(
      appBar: AppBar(
        title: Text('Score Counter'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.all(20),
            child: Text(
              ("Masukan Skor Tiap Team"),
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          ),
          Container(
            margin: EdgeInsets.all(20),
            child: TextField(
              decoration: InputDecoration(
                labelText: 'Skor Team A',
              ),
              onChanged: (String inputText) {
                kalimat1 = inputText;
              },
            ),
          ),
          Container(
            margin: EdgeInsets.all(20),
            child: TextField(
              decoration: InputDecoration(
                labelText: 'Skor Team B',
              ),
              // setState
              onChanged: (String inputText) {
                kalimat2 = inputText;
              },
            ),
          ),
          FlatButton(
            onPressed: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => ContentPage(
                          skor1: kalimat1,
                          skor2: kalimat2,
                          kata: '',
                          color: Colors.red)));
            },
            child: Text("Lihat Skor"),
            color: Colors.tealAccent,
          ),
          Row(children: [
            FloatingActionButton(
                heroTag: 'btn1',
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => ContentPage(
                              skor1: kalimat1,
                              skor2: kalimat2,
                              kata: '',
                              color: Colors.red)));
                  setState(() {
                    angka++;
                  });
                },
                child: Icon(Icons.add),
                backgroundColor: Colors.blue),
            FloatingActionButton(
                heroTag: 'btn2',
                onPressed: () {
                  setState(() {
                    angka--;
                  });
                },
                child: Icon(Icons.remove),
                backgroundColor: Colors.red),
          ]),
        ],
      ),
    );
  }
}
