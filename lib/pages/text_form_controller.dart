import 'package:flutter/material.dart';

class TextFormController extends StatefulWidget {
  _TextFormController createState() => _TextFormController();
}

class _TextFormController extends State<TextFormController> {
  TextEditingController _controller;

  // ignore: must_call_super
  void initState() {
    _controller = TextEditingController();
    _controller.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.all(20),
            child: Text(_controller.text),
          ),
          Container(
            margin: EdgeInsets.all(20),
            child: TextField(
              controller: _controller,
              // ignore: non_constant_identifier_names
              onSubmitted: (Text) {
                setState(() {});
              },
            ),
          ),
        ],
      ),
    );
  }
}
