import 'package:flutter/material.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Home Page"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FlatButton(
                onPressed: () {
                  Navigator.pushNamed(context, content());
                },
                child: Text("Menu Konten"),
                color: Colors.green,
                padding: EdgeInsets.all(8.0),
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/counter');
                },
                child: Text("Menu Counter"),
                color: Colors.blue,
              ),
              FlatButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/text-form');
                },
                child: Text("Text Form"),
                color: Colors.blue,
              ),
            ],
          ),
        ));
  }

  String content() => '/content';
}
