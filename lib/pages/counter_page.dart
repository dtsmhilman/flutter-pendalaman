import 'package:flutter/material.dart';

class CounterPage extends StatefulWidget {
  @override
  _CounterPage createState() => _CounterPage();
}

class _CounterPage extends State<CounterPage> {
  _CounterPage({this.title});

  String title;
  int angka = 0;
  int angka1 = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Counter Page'),
      ),
      body: Center(
        child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Text(
            "Skor Team A = " + angka.toString(),
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
          ),
          Text(
            "Skor Team B = " + angka1.toString(),
            style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
          ),
          SizedBox(
            height: 40,
          ),
          Row(children: [
            FloatingActionButton(
                heroTag: 'btn1',
                onPressed: () {
                  setState(() {
                    angka++;
                  });
                },
                child: Icon(Icons.add),
                backgroundColor: Colors.blue),
            FloatingActionButton(
                heroTag: 'btn2',
                onPressed: () {
                  setState(() {
                    angka--;
                  });
                },
                child: Icon(Icons.remove),
                backgroundColor: Colors.red),
          ]),
          Row(children: [
            FloatingActionButton(
                heroTag: 'btn3',
                onPressed: () {
                  setState(() {
                    angka1++;
                  });
                },
                child: Icon(Icons.add),
                backgroundColor: Colors.blue),
            FloatingActionButton(
                heroTag: 'btn4',
                onPressed: () {
                  setState(() {
                    angka1--;
                  });
                },
                child: Icon(Icons.remove),
                backgroundColor: Colors.red),
          ]),
        ]),
      ),
    );
  }
}
