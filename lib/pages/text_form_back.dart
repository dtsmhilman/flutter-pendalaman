import 'package:flutter/material.dart';

class TextFormBack extends StatefulWidget {
  @override
  _TextFormBack createState() => _TextFormBack();
}

class _TextFormBack extends State<TextFormBack> {
  String kalimat = "";
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Text FormBack'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            margin: EdgeInsets.all(20),
            child: Text(kalimat),
          ),
          FlatButton(
            onPressed: () {
              Navigator.pushNamed(context, '/text-form');
            },
            child: Text("Go Back"),
            color: Colors.tealAccent,
          ),
        ],
      ),
    );
  }
}
