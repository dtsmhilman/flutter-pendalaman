import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ContentPage extends StatelessWidget {
  ContentPage({
    this.skor1,
    this.skor2,
    this.kata,
    this.color,
  });

  String skor1;
  String skor2;
  String kata;
  Color color;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Score Counter"),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                ("SCORE TEAM A = ") + this.skor1,
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
              Text(
                ("SCORE TEAM B = ") + this.skor2,
                style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
              ),
              Text(this.kata),
              FlatButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text("Mulai Ulang"),
                  color: Colors.yellow[900])
            ],
          ),
        ));
  }
}
