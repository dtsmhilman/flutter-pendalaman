import 'package:flutter/material.dart';
import 'package:pendalaman_flutter_skor/screens/name.dart';
import 'package:pendalaman_flutter_skor/screens/score.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Papan Skor Tim - DTS FE UI',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      initialRoute: '/team_name',
      routes: {
        '/team_name': (context) => TeamName(),
        '/team_score': (context) => TeamScore(),
      },
      debugShowCheckedModeBanner: false,
    );
  }
}
